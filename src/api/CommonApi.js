import axios from 'axios';

export const _getHeader = async () => {
  const fakeToken = 'f4c7dd6f73ddfd0d1002b7b843bbdadb';
  const language = 'vn';
  const headers = {
    Authorization: 'jwt ' + fakeToken,
    language: language,
  };
  return headers;
};

export const _checkSuccess = async response => {
  if (!response) {
    const response = {
      data: {message: 'unknown error'},
    };
  }
  return response.data;
};

export const _checkError = async error => {
  console.error('send API error: ', error);
};

export const sendApi = async (method, data, url) => {
  const config = {
    method: method,
    header: await _getHeader(),
    url: url,
  };
  await axios(config)
    .then(response => _checkSuccess(response))
    .catch(error => _checkError(error));
};

export const sendGetApi = async (url, data) => {
  return await sendApi('get', data, url);
};

export const sendPostApi = async (url, data) => {
  return await sendApi('post', data, url);
};

export const sendPutApi = async (url, data) => {
  return await sendApi('put', data, url);
};

export const sendDelete = async (url, data) => {
  return await sendApi('delete', data, url);
};
