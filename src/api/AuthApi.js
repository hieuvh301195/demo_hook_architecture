import {sendPostApi} from './CommonApi';
import {LOGIN} from './Endpoint';

export const login = async (username, password) => {
  const data = {
    username: username,
    password: password,
  };
  const response = await sendPostApi(LOGIN, data);
};
