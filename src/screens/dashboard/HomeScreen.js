import React, {useState} from 'react';
import {
  View,
  StyleSheet,
  Text,
} from 'react-native';
import Header from '../../components/Header';
import {totalHeaderHeight} from '../../constant/Layout';

function HomeScreen() {
  return (
    // eslint-disable-next-line react/jsx-no-undef
    <View style={styles.container}>
      <Header title={'Dashboard'} />
      <Text>Home Screen</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: totalHeaderHeight,
  },
});

export default HomeScreen;
