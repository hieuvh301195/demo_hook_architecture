import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
} from 'react-native';
import Header from '../../components/Header';
import {totalHeaderHeight} from '../../constant/Layout';

function AccountScreen() {
  return (
    // eslint-disable-next-line react/jsx-no-undef
    <View style={styles.container}>
      <Header title={'Account'} />
      <Text>Account Screen</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: totalHeaderHeight,
  },
});

export default AccountScreen;
