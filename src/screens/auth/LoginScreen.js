import React, {useState} from 'react';
import {View, StyleSheet, TouchableOpacity, Text} from 'react-native';
import * as Layout from '../../constant/Layout';
import {HOME, TAB_NAVIGATOR} from '../../navigation/NameNavigators';
import Loading from '../../components/Loading';

const {width, height} = Layout.window;

function LoginScreen(props) {
  const [loading, setLoading] = useState(false);

  const _navigateToTabBar = () => {
    _disableLoading();
    props.navigation.navigate(TAB_NAVIGATOR);
  };

  const _enableLoading = () => setLoading(true);

  const _disableLoading = () => setLoading(false);

  const _onPressToLoginButton = () => {
    _enableLoading();
    setTimeout(() => _navigateToTabBar(), 2000);
  };

  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={styles.button}
        onPress={() => _onPressToLoginButton()}>
        <Text style={styles.buttonText}>Move to tab bar</Text>
      </TouchableOpacity>
      <Loading isLoading={loading} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FAFAFA',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  button: {
    width: width * 0.4,
    height: height / 15,
    backgroundColor: '#0077CC',
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {
    color: '#FFF',
  },
});

export default LoginScreen;
