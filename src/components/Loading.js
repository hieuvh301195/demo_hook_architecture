import React, {useState, useEffect} from 'react';
import {View, ActivityIndicator, StyleSheet, Modal, Text} from 'react-native';
import * as Layout from '../constant/Layout';
import {ENABLED_COLOR} from '../constant/Colors';

const {width, height} = Layout.window;

export default function Loading(props) {
  const loading = props.isLoading

  if (loading) {
    return (
      <View style={styles.container}>
        <View style={styles.loadingContainer}>
          <ActivityIndicator size={'large'} color={ENABLED_COLOR} />
          <Text style={styles.textLoading}>processing</Text>
        </View>
      </View>
    );
  }
  return <View />;
}

const styles = StyleSheet.create({
  container: {
    width: width,
    height: height,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#00000099',
    position: 'absolute',
    elevation: 8,
  },
  loadingContainer: {
    width: width * 0.8,
    height: height / 10,
    backgroundColor: '#FFF',
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 8,
    paddingLeft: 10,
    opacity: 1,
  },
  textLoading: {
    fontSize: 14,
    marginLeft: 10,
  },
});
