import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  StatusBar,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import * as Layout from '../constant/Layout';

const {width, height} = Layout.window;
const statusBarHeight = StatusBar.currentHeight;
const iconSize = 24;

export default function Header(props) {
  const _getLeftIconName = () => {
    if (props.type && props.type === 2) {
      return 'arrow-left';
    }
    return 'menu';
  };

  const _openDrawerMenu = () => {
    props.navigation.openDrawer();
  };

  return (
    <View style={styles.container}>
      <View style={styles.menuContainer}>
        <TouchableOpacity onPress={() => _openDrawerMenu()}>
          <Icon name={_getLeftIconName()} size={iconSize} />
        </TouchableOpacity>
        <Text>{props.title}</Text>
        <TouchableOpacity>
          <Icon name={'email-outline'} size={iconSize} />
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: width,
    height: statusBarHeight * 2.5,
    backgroundColor: '#0098cc',
    position: 'absolute',
    top: 0,
    paddingTop: statusBarHeight,
  },
  menuContainer: {
    width: width,
    height: statusBarHeight * 1.5,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 15,
  },
});
