import {Dimensions, Platform, StatusBar} from 'react-native';

export const window = {
  width: Dimensions.get('window').width,
  height: Dimensions.get('window').height,
};

export const isAndroid = Platform.OS === 'android';
export const isIOS = Platform.OS === 'ios';
export const statusBarHeight = StatusBar.currentHeight;
export const totalHeaderHeight = statusBarHeight * 2.5;
export const headerHeight = statusBarHeight * 1.5;
