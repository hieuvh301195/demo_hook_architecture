import * as TYPE from '../ActionTypes';

export const increase = number => {
  return {
    type: TYPE.INCREMENT,
    number: number,
  };
};

export const decrease = number => {
  return {
    type: TYPE.DECREMENT,
    number: number,
  };
};
