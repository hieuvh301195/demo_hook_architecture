import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {
  DRAWER_NAVIGATOR,
  LOGIN,
  SIGN_UP,
  TAB_NAVIGATOR,
} from './NameNavigators';
import LoginScreen from '../screens/auth/LoginScreen';
import SignUpScreen from '../screens/auth/SignUpScreen';
import TabNavigator from './TabNavigator';
import DrawerNavigator from './DrawerNavigator';

const Stack = createStackNavigator();

export default function MainNavigator() {
  return (
    <NavigationContainer>
      <Stack.Navigator headerMode={'none'}>
        <Stack.Screen name={LOGIN} component={LoginScreen} />
        <Stack.Screen name={SIGN_UP} component={SignUpScreen} />
        <Stack.Screen name={TAB_NAVIGATOR} component={TabNavigator} />
        <Stack.Screen name={DRAWER_NAVIGATOR} component={DrawerNavigator} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
