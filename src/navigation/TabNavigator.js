import * as React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {ACCOUNT, HOME, SETTINGS} from './NameNavigators';
import * as COLORS from '../constant/Colors';
import HomeScreen from '../screens/dashboard/HomeScreen';
import SettingScreen from '../screens/dashboard/SettingScreen';
import AccountScreen from '../screens/dashboard/AccountScreen';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Header from '../components/Header';

const TabBar = createBottomTabNavigator();
const iconSize = 18;

const _getIcon = (name, focused) => {
  switch (name) {
    case HOME:
      return (
        <Icon
          name={'home'}
          color={focused ? COLORS.ENABLED_COLOR : COLORS.DISABLED_COLOR}
          size={iconSize}
        />
      );
    case SETTINGS:
      return (
        <Icon
          name={'cogs'}
          color={focused ? COLORS.ENABLED_COLOR : COLORS.DISABLED_COLOR}
          size={iconSize}
        />
      );
    case ACCOUNT:
      return (
        <Icon
          name={'account'}
          color={focused ? COLORS.ENABLED_COLOR : COLORS.DISABLED_COLOR}
          size={iconSize}
        />
      );
  }1
};

export default function TabNavigator() {
  return (
    <TabBar.Navigator
      screenOptions={({route}) => ({
        tabBarIcon: ({focused}) => {
          return _getIcon(route.name, focused);
        },
      })}
      tabBarOptions={{
        activeTintColor: COLORS.ENABLED_COLOR,
        inactiveTintColor: COLORS.DISABLED_COLOR,
      }}
      lazy={true}>
      <TabBar.Screen name={SETTINGS} component={SettingScreen} />
      <TabBar.Screen name={HOME} component={HomeScreen} />
      <TabBar.Screen name={ACCOUNT} component={AccountScreen} />
    </TabBar.Navigator>
  );
}
