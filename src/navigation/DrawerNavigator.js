import {createDrawerNavigator} from '@react-navigation/drawer';
import React from 'react';
import {ACCOUNT, SETTINGS} from './NameNavigators';
import SettingScreen from '../screens/dashboard/SettingScreen';
import AccountScreen from '../screens/dashboard/AccountScreen';

const Drawer = createDrawerNavigator();

export default function DrawerNavigator() {
  return (
    <Drawer.Navigator initialRouteName={SETTINGS}>
      <Drawer.Screen name={SETTINGS} component={SettingScreen} />
      <Drawer.Screen name={ACCOUNT} component={AccountScreen} />
    </Drawer.Navigator>
  );
}
