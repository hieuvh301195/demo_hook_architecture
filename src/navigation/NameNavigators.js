export const LOGIN = 'login';
export const SIGN_UP = 'signUp';
export const HOME = 'home';
export const ACCOUNT = 'account';
export const SETTINGS = 'settings';
export const CREATE_MEETING = 'createMeeting';
export const TAB_NAVIGATOR = 'tabNavigator';
export const DRAWER_NAVIGATOR = 'drawerNavigator';
